const express = require('express');
const router = express.Router();
const userController = require('../controllers/userCtrl');
const {checkSignup} = require('../middleware/validator');

router.post('/signup', checkSignup, userController.signup);
router.post('/login', userController.login);

module.exports = router;