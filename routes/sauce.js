const express = require('express');
const router = express.Router();
const {auth} = require('../middleware/auth');

const sauceController = require('../controllers/sauceCtrl');
const multer = require('../middleware/multer');
const {checkForm, parse} = require('../middleware/validator');

router.post('/', auth, multer, parse, checkForm, sauceController.createSauce);
router.get('/:id', auth, sauceController.getOneSauce);
router.delete('/:id', auth, sauceController.deleteSauce);
router.put('/:id', auth, multer, parse, checkForm, sauceController.updateSauce);
router.post('/:id/like', auth, sauceController.updateLike);
router.get('/', auth, sauceController.getAllSauces);

module.exports = router;