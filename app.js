const express = require('express');
const mongoose = require('mongoose');
const path = require('path');

const app = express();
const dotenv = require('dotenv');
dotenv.config();

const rateLimit = require('express-rate-limit');
const limiter = rateLimit({
  windowMs: 15 * 60 * 100,
  max: 100
});

const userRoutes = require('./routes/user');
const saucesRoutes = require('./routes/sauce');

mongoose.connect(process.env.DB_CONNECT,
    { useNewUrlParser: true,
    useUnifiedTopology: true})
    .then(() => console.log('Connexion to MongoDB successful'))
    .catch((error) => console.log('Failed to connect to MongoDB ' + error));

app.disable('x-powered-by');

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
  });

app.use(express.json());

app.use(limiter);

app.use('/images', express.static(path.join(__dirname, 'images')));
app.use('/api/auth', userRoutes);
app.use('/api/sauces', saucesRoutes);

module.exports = app;