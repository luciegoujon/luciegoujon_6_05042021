const jwt = require('jsonwebtoken');

exports.auth = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        const decodedToken = jwt.verify(token, 'NyBQmJ3ezMkEzu1KuTVz');
        const userId = decodedToken.userId;
        if (req.body.userId && req.body.userId !== userId) {
            throw 'wrong user ID';
        } else {
            next();
        }
    } catch (error) {
        res.status(401).json({error});
    }
};

exports.userIdDecoded = (req, res, next) => {
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.verify(token, 'NyBQmJ3ezMkEzu1KuTVz');
    return String(decodedToken.userId);
}