const {validationResult, check} = require('express-validator');

exports.checkSignup = [
    check('email').isEmail().trim(),
    check('password').isLength({min: 6}),
    (req, res, next) => {
        const errors = validationResult(req);

        if (!errors.isEmpty())
            return res.status(422).json({error: errors.array()});
        next();
    },
];

exports.checkForm = [
    check('name').escape().trim().isLength({min: 2}),
    check('manufacturer').escape().trim().isLength({min: 2}),
    check('description').escape().trim().isLength({min: 2}),
    check('mainPepper').escape().trim().isLength({min: 2}),
    (req, res, next) => {
        const errors = validationResult(req);

        if (!errors.isEmpty())
            return res.status(422).json({error: errors.array()});
        next();
    },
]

exports.parse = (req, res, next) => {
    if (req.body.sauce) {
        req.body = JSON.parse(req.body.sauce);
    }
    next();
}
