const User = require('../models/User');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const cryptoJS = require('crypto-js');

exports.signup = (req, res, next) => {
    bcrypt.hash(req.body.password, 10)
        .then(hash => {
            const cryptedEmail = cryptoJS.SHA256(req.body.email).toString();
            const user = new User({
               email: cryptedEmail,
               password: hash
           });
           user.save()
            .then(() => res.status(201).json({ message: 'User created' }))
            .catch(error => res.status(400).json({ error }));
        })
        .catch(error => res.status(500).json({ error }));
};

exports.login = (req, res, next) => {
    const cryptedEmail = cryptoJS.SHA256(req.body.email).toString();
    User.findOne({ email: cryptedEmail })
        .then(user => {
            if (!user) {
                return res.status(401).json({ error: 'authentification failed'})
            }
            bcrypt.compare(req.body.password, user.password)
                .then(valid => {
                    if (!valid) {
                        return res.status(401).json({ error: 'authentification failed'})
                    }
                    res.status(200).json({
                        userId: user._id,
                        token: jwt.sign(
                            { userId: user._id},
                            'NyBQmJ3ezMkEzu1KuTVz',
                            { expiresIn: '24h'}
                        )
                    });
                })
                .catch(error => res.status(500).json({ error }));
        })
        .catch(error => res.status(500).json({ error }));
};