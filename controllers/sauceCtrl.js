const fs = require('fs');
const Sauce = require('../models/Sauce');
const {userIdDecoded} = require('../middleware/auth');

exports.getAllSauces = (req, res, next) => {
    Sauce.find()
        .then(sauces => res.status(200).json(sauces))
        .catch(error => res.status(404).json({error}));
};

exports.createSauce = (req, res, next) => {
    const sauceObject = req.body;
    const sauce = new Sauce({
        ...sauceObject,
        imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`,
        likes: 0,
        dislikes: 0,
        usersLiked: [],
        usersDisliked: []
    });
    sauce.save()
        .then( () => res.status(201).json({message: 'sauce added'}))
        .catch( error => res.status(400).json({ error }));
};

exports.getOneSauce = (req, res, next) => {
    Sauce.findOne({ _id: req.params.id })
        .then(sauce => res.status(200).json(sauce))
        .catch(error => res.status(404).json({ error }));
};

exports.deleteSauce = (req, res, next) => {
    Sauce.findOne({_id: req.params.id})
        .then(sauce => {
            // only original uploader allowed to delete
            const idUser = userIdDecoded(req, res, next);
            if (idUser === sauce.userId) {
                const filename = sauce.imageUrl.split('/images/')[1];
                fs.unlink(`images/${filename}`, () => {
                    Sauce.deleteOne({ _id: req.params.id })
                        .then( () => res.status(200).json({message: 'sauce deleted'}))
                        .catch(error => res.status(400).json({ error }));
                })
           } else {
                return res.status(401).json({error : 'forbidden'});
            }
        })
        .catch(error => res.status(500).json({ error }));
};

exports.updateSauce = (req, res, next) => {
    Sauce.findOne({_id: req.params.id})
        .then(sauce => {
            // only original uploader allowed to modify
            if (req.body.userId === sauce.userId) {
                let sauceObject = {};
                if (req.file) {
                    sauceObject = 
                    { 
                        ...req.body,
                        imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
                    };
                    // delete previous image
                    const filename = sauce.imageUrl.split('/images/')[1];
                    fs.unlink(`images/${filename}`, () => {});
                        
                } else {
                    sauceObject = { ...req.body };
                }
                Sauce.updateOne({ _id: req.params.id}, { ...sauceObject, _id: req.params.id})
                    .then(() => res.status(200).json({ message: 'sauce updated'}))
                    .catch(error => res.status(400).json({error}));
            } else {
                return res.status(401).json({error : 'forbidden'}); 
            }
        })
        .catch(error => res.status(500).json({ error }));
};

exports.updateLike = (req, res, next) => {
    Sauce.findOne({_id: req.params.id})
        .then(sauce => {
            const userId = req.body.userId;
            const like = req.body.like;

            if (sauce.usersLiked.includes(userId) === true && sauce.usersDisliked.includes(userId) === false && like === 0) {
                // case 0 cancel like
                Sauce.updateOne({ _id: req.params.id}, { $pull: { usersLiked: userId}, $inc: {likes: -1} })
                    .then( () => res.status(200).json({ message: 'likes updated' }))
            }
            else if (sauce.usersDisliked.includes(userId) === true && sauce.usersLiked.includes(userId) === false && like === 0) {
                // case 0 cancel dislike
                Sauce.updateOne({ _id: req.params.id}, { $pull: { usersDisliked: userId}, $inc: {dislikes: -1} })
                    .then( () => res.status(200).json({ message: 'dislikes updated' })) 
            }
            else if (sauce.usersLiked.includes(userId) === false && sauce.usersDisliked.includes(userId) === false) {
                if (like === 1) {
                    // case 1 like
                    Sauce.updateOne({ _id: req.params.id}, { $push: { usersLiked: userId}, $inc: {likes: 1} })
                        .then( () => res.status(200).json({ message: 'likes updated' })) 
                }
                else if (like === -1) {
                    // case -1 dislike
                    Sauce.updateOne({ _id: req.params.id}, { $push: { usersDisliked: userId}, $inc: {dislikes: 1} })
                        .then( () => res.status(200).json({ message: 'dislikes updated' }))
                }
                else {
                    return res.status(400).json({ error: 'bad request'});
                }
            }
            else {
                return res.status(400).json({ error: 'bad request'});
            }
        })
        .catch(error => res.status(500).json({error}));
}
